<?php

use App\Http\Controllers\TweetController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\HashtagController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'TweetController@index')->name('home');
Route::post('/', 'TweetController@store')->name('storeTweet');
Route::get('/tweet/{id}', 'TweetController@show')->name('showTweet');
Route::delete('/tweet/{id}', 'TweetController@destroy')->name('destroyTweet');
Route::post('/tweet/{id}/comment', 'CommentController@store')->name('storeComment');
Route::get('/tweet/{id}/vote', 'VoteController@store')->name('toggleLike');
Route::get('/hashtag/{id}', 'HashtagController@show')->name('tweetsByHashtag');
Route::get('/user/{id}', 'UserController@show')->name('userTweet');
Route::get('/users', 'UserController@index')->name('users');
Route::get('/user/{id}/activate', 'UserController@activate')->name('activateUser');
Route::get('/user/{id}/deactivate', 'UserController@deactivate')->name('deactivateUser');
Route::post('/avatar', 'UserController@storeAvatar')->name('uploadUser');
