<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Hashtag;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['only' => ['index'] ]);
        $this->middleware('isVerified');
        $this->middleware('admin', ['only' => ['activate', 'deactivate'] ]);
    }

    public function index() {
        if (!Auth::user()->hasRole('staff')) {
            return redirect('/');
        }
        return view('admin.users')->with('users', User::all());
    }

    public function show($id) {
        if (!Auth::check() && !User::find($id)->hasRole('staff')) {
            return redirect('/');
        }
        return view('tweet.list')->with('tweets', User::find($id)->tweets()->orderBy('created_at', 'desc')->get())->with('hashtags', Hashtag::orderBy('updated_at', 'desc')->take(10)->get())->with('user', User::find($id));
    }

    public function activate($id) {
        $user = User::find($id);
        $user->assignRole('scout');
        $user->verified = true;
        $user->save();
        return back();
    }

    public function deactivate($id) {
        $user = User::find($id);
        $user->removeRole('scout');
        $user->verified = false;
        $user->save();
        return back();
    }

    public function storeAvatar(Request $request) {
        var_dump($request->hasFile('avatar'));
        if($request->hasFile('avatar')){
            $user = Auth::user();
            $user->avatar = $request->file('avatar')->store('avatars');
            $user->save();
        }
        return back();
    }
}
