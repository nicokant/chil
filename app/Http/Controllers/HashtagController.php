<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hashtag;
use Auth;

class HashtagController extends Controller
{
    public function __construct() {
        $this->middleware('isVerified');
    }

    public function show($id) {
        $tweets = Hashtag::where(['name'=>strtolower($id)])->first()->tweets()->orderBy('created_at', 'desc');

        if (!Auth::check()) {
            $tweets->where('public', true);
        }
        return view('tweet.list')->with('tweets', $tweets->get())->with('hashtags', Hashtag::orderBy('updated_at', 'desc')->take(10)->get());
    }
}
