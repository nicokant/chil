<?php

namespace App\Http\Controllers;

use App\Comment;
use Auth;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct() {
        $this -> middleware('auth');
        $this -> middleware('isVerified');
    }

    public function store($id, Request $request) {
        $comment = new Comment;
        $comment -> user_id = Auth::user() -> id;
        $comment -> tweet_id = $id;
        $comment -> text = $request -> input('text');
        $comment -> save();

        return back();
    }
}
