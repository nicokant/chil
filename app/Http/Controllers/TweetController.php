<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Tweet;
use App\Hashtag;

class TweetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', [ 'except' => [ 'index', 'show', 'list' ]]);
        $this->middleware('isVerified', ['except' => [ 'index', 'list']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::check() && Auth::user()->verified) {
            $tweets = Tweet::orderBy('created_at', 'desc');
        } else {
            $tweets = Tweet::where('public', true)->orderBy('created_at', 'desc');
        }
        return view('tweet/list')->with('tweets', $tweets->get())->with('hashtags', Hashtag::orderBy('updated_at', 'desc')->take(10)->get())->with('user', Auth::user());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hashregex = "/\B#(\w*[a-zA-Z]+\w*)/";
        $tweet = new Tweet;
        $tweet->body = $request->input('body');
        $tweet->user_id = Auth::user()->id;
        if ($request->file('image')) {
            $tweet->image_url = $request->file('image')->store('images');
        }
        $tweet->public = Auth::user()->hasRole('staff');
        $tweet->save();

        if (preg_match_all($hashregex, $tweet->body, $matches) > 0) {
            foreach ($matches[1] as $index => $hash) {
                $hashtag = Hashtag::firstOrNew(['name' => strtolower($hash)]);
                $tweet -> hashtags() -> save($hashtag);
            }
        }

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('tweet/show')->with('tweet', Tweet::find($id))->with('hashtags', Hashtag::orderBy('updated_at', 'desc')->take(10)->get());
    }

    public function destroy($id)
    {
        /* TODO: Check if user is authorized */
        Tweet::find($id)->delete();
        return redirect('/');
    }

    public function list() {
        return Tweet::where('public', true)->orderBy('created_at', 'desc')->with('user')->with('votes')->with('comments.user')->get();
    }
}
