<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vote;
use Auth;

class VoteController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isVerified');
    }

    public function store($tweet_id) {
        $vote = ['tweet_id'=>$tweet_id, 'user_id'=>Auth::user()->id];
        if (Vote::where($vote)->exists()) {
            Vote::where($vote)->first()->delete();
        } else {
            Vote::create($vote)->save();
        }

        return back();
    }
}
