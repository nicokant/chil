<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'user_id', 'tweet_id'
    ];

    public function users() {
        return $this->belongsTo('App\User');
    }

    public function tweets() {
        return $this->belongsTo('App\Tweet');
    }
}
