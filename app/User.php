<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Auth;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $guard_name = 'web';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tweets() {
        return $this->hasMany('App\Tweet');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function votes() {
        return $this->hasMany('App\Vote');
    }

    public static function hasVoted($tweetId) {
        return Auth::check() && Vote::where(['tweet_id' => $tweetId, 'user_id' => Auth::user()->id])->exists();
    }

    public function getAvatarUrlAttribute() {
        if ($this->avatar) {
            return asset("storage/{$this->avatar}");
        } else {
            return "https://api.adorable.io/avatars/500/{$this->name}";
        }
    }
}
