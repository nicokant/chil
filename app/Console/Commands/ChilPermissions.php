<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;

class ChilPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chil:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(! Role::where('name', 'staff')->exists()) {
            Role::create(['name'=>'staff']);
        }
        if(! Role::where('name', 'scout')->exists()) {
            Role::create(['name'=>'scout']);
        }
    }
}
