<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class ChilAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chil:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate admin account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User();
        $user->name = 'Admin';
        $user->password = Hash::make('siamodunostessosangetuedio');
        $user->email = 'admin@chil.com';
        $verified->true;
        $user->save();

        if(! Role::where('name', 'staff')->exists()) {
            Role::create(['name'=>'staff']);
        }
        $user->assignRole('staff');
    }
}
