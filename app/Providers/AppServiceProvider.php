<?php

namespace App\Providers;

use Blade;
use App\Http\Controllers\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Blade::directive('owner', function($owner_field) {
            return "<?php if(Auth::check() && Auth::user()->id == $owner_field): ?>";
        });
        Blade::directive('endowner', function() {
            return "<?php endif; ?>";
        });
        Blade::directive('avatar', function($string) {
            return "<?php echo 'https://api.adorable.io/avatars/500/'.$string; ?>";
        });
        Blade::directive('userNotVerified', function() {
            return "<?php if(Auth::check() && !Auth::user()->verified): ?>";
        });
        Blade::directive('endNotVerified', function() {
            return "<?php endif; ?>";
        });
    }
}
