
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(".tweet-body").html(function(_, html) {
   return  html.replace(/\B#(\w*[a-zA-Z]+\w*)/g, '<a href="/hashtag/$1" class="text-dark font-weight-bold">#$1</a>')
});

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#img-preview').attr('src', e.target.result);
      $('#preview-wrapper').toggleClass('d-none').toggleClass('d-flex');
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#img-upload").change(function() {
  if ($(this).val() !== "") {
    readURL(this);
  }
});

$('#remove-img').click(function() {
  $("#img-upload").val('');
  $('#img-preview').attr('src', '');
  $('#preview-wrapper').toggleClass('d-none').toggleClass('d-flex');
})

$('#createTweet').focusout(function() {
  if ($('#createTweet textarea').val() === "") {
    $('#createTweet').addClass('compress');
  }
});

$('#createTweet textarea').focusin(function() {
  if ($('#createTweet textarea').val() === "") {
    $('#createTweet').removeClass('compress');
  }
});

$('#avatar-link').click(function() {
  $('#avatar-upload').click();
});

$('#avatar-upload').change(function() {
  $('#avatar-upload-form').submit();
});
