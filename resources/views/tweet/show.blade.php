@extends('tweet/index')

@section('main')
    @include('tweet.components.tweet', ['showComments' => true])
@endsection
