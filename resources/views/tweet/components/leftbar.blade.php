@guest

@else
    @if(isset($user))
        <div class="card bg-info no-border">
            <img class="card-img-top" src="{{ $user -> avatarUrl }}" />
            <div class="card-body d-flex flex-column align-items-center">
                <h4 class="card-title">{{ $user -> name }}
                    <small><div class="dropdown float-right">
                      <a class="dropdown-toggle" href="#" role="button" id="dropdownAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownAvatar">
                            <a class="dropdown-item" id="avatar-link" href="#">Carica foto</a>
                      </div>
                    </div></small>
                </h4>
                @userNotVerified
                    <p class="card-text"><i class="fa fa-flag"></i> In attesa di verifica</p>
                @endNotVerified
                <div class="d-flex justify-content-between mt-4 w-100">
                    <div class="text-center"><strong>Tweets<br />{{ $user -> tweets -> count() }}</strong></div>
                    <div class="text-center"><strong>Likes <br />{{ $user -> votes -> count() }}</strong></div>
                </div>
            </div>
        </div>
        {!! Form::open(['action' => 'UserController@storeAvatar', 'files' => true, 'id'=>'avatar-upload-form', 'hidden'=>'hidden']) !!}
            {!! Form::token() !!}
            {!! Form::file('avatar', array('id'=>'avatar-upload')) !!}
        {!! Form::close() !!}
    @endif
@endguest

