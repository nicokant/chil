@guest
    <div class="card bg-info my-2 no-border">
        <div class="card-body">
            <h3 class="card-title">Accedi</h3>
            <p class="card-text"><a href="{{ route('login') }}">Login</a></p>
            <p class="card-text"><a href="{{ route('register') }}">Register</a></p>
        </div>
    </div>
@else
    @userNotVerified
        <div class="card bg-info mt-2 no-border">
            <div class="card-body">
                Sei in attesa di essere verificato da un amministratore
            </div>
        </div>
    @else
        <div class="card bg-info no-border">
            <div class="card-body">
                <h4 class="mb-4">Tendenze</h4>
                @foreach($hashtags as $hashtag)
                    <p class="card-text"><a href="{{ URL::route('tweetsByHashtag', ['id' => $hashtag -> name]) }}" class="text-dark">#{{ $hashtag -> name }}</a></p>
                @endforeach
            </div>
        </div>
    @endif
@endguest
