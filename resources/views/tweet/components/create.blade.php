<div id="createTweet" class="card bg-info no-border compress">
    <div class="card-body">
        {!! Form::open(['action' => 'TweetController@store', 'files' => true]) !!}
            {!! Form::token() !!}
            <div>
                {!! Form::textarea('body', null,
                    array('required',
                          'maxlength' => '280',
                          'class'=>'form-control',
                          'placeholder'=>'Raccontaci qualcosa!')) !!}
            </div>
            <div id="preview-wrapper" class="justify-content-end d-none">
              <div class="rounded border border-primary p-2 mt-2">
                <img id="img-preview" class="rounded img-small" />
                <span id="remove-img" class="mx-1 text-primary"><i class="fa fa-close"></i></span>
              </div>
            </div>
            <div id="actions" class="d-flex justify-content-between py-2">
              <div>
                  {!! Form::submit('Invia', array('class'=>'btn btn-primary float-right')) !!}
              </div>
              <label class="btn btn-file btn-outline-primary btn-sm">
                <i class="fa fa-camera"></i> {!! Form::file('image', array('id'=>'img-upload', 'style'=>'display:none;')) !!}
              </label>
            </div>
        {!! Form::close() !!}
    </div>
</div>
