{!! Form::open(['action' => ['CommentController@store',  $tweet -> id], 'class' => 'd-flex']) !!}
    {!! Form::token() !!}
    <div class="form-group mr-1" style="flex:1">
        {!! Form::text('text', null,
            array('required',
                  'maxlength' => '140',
                  'class'=>'form-control',
                  'placeholder'=>'Commento...')) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Invia', array('class'=>'btn btn-primary float-right')) !!}
    </div>
{!! Form::close() !!}
