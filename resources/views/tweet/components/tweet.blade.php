<div class="card my-2 bg-info tweet no-border">
    <div class="card-body">
        <div class="media">
            <img class="avatar-thumbnail rounded mr-3" src="{{ $tweet -> user -> avatarUrl }}"/>
            <div class="media-body">
                <div>
                    <a href="{{ URL::route('userTweet', ['id'=> $tweet -> user -> id]) }}" class="text-dark font-weight-bold">{{ $tweet -> user -> name }}</a>
                    <small class="text-muted ml-2">{{ $tweet -> created_at -> diffForHumans() }}</small>

                    @owner($tweet-> user ->id)
                    <div class="dropdown float-right">
                      <a class="dropdown-toggle" href="#" role="button" id="dropdownTweet{{ $tweet -> id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownTweet{{ $tweet -> id }}">
                            <a class="dropdown-item" href="#" data-tweet-delete="{{ $tweet -> id }}">Elimina</a>
                      </div>
                    </div>
                    @endowner

                </div>
                <p class="card-text tweet-body">{{ $tweet -> body }}</p>
                @if ($tweet -> image_url)
                    <img class="img-fluid rounded mx-auto d-block" src="{{ asset('storage/'.$tweet -> image_url) }}" />
                @endif
                <div class="d-flex mt-2">
                    <a href="{{ URL::route('showTweet', ['id'=>$tweet->id]) }}" class="btn mx-1"><i class="fa fa-comment-o"></i> {{ $tweet -> comments -> count() }}</a>
                    <a href="
                    @guest
                        {{ URL::route('showTweet', ['id'=>$tweet->id]) }}
                    @else
                        {{ URL::route('toggleLike', ['id'=>$tweet->id]) }}
                    @endguest
                    " class="btn mx-1"><i class="fa
                    @if (User::hasVoted($tweet -> id))
                        fa-heart
                    @else
                        fa-heart-o
                    @endif
                    "></i> {{ $tweet -> votes -> count() }}</a>
                </div>
                @if (isset($showComments) && $showComments)
                    <ul class="list-group list-group-flush mt-2 ">
                        @foreach ($tweet->comments()->orderBy('created_at', 'asc')->get() as $comment)
                            <li class="list-group-item bg-info">
                                @include('tweet.components.comment')
                            </li>
                        @endforeach
                        @if (Auth::check() && Auth::user()->verified)
                            <li class="list-group-item bg-info">
                                @include('tweet.components.create_comment')
                            </li>
                        @endif
                    </ul>
                @endif
            </div>
        </div>
    </div>
</div>
