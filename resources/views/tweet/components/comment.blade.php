<div class="media">
    <img class="avatar-thumbnail rounded mr-3" src="{{ $comment -> user -> avatarUrl }}"/>
    <div class="media-body">
        <strong class="mr-2">{{ $comment -> user -> name }}</strong> {{ $comment -> text }}
    </div>
</div>
