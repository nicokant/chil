@extends('tweet/index')

@section('main')
    @if(Auth::check() && Auth::user()->hasanyrole('staff|scout'))
        @include('tweet.components.create')
    @endif

    @each('tweet.components.tweet', $tweets, 'tweet', 'tweet.components.empty_tweet')
@endsection
