@extends('../layouts/app')

@section('content')
    <div class="row mt-2 d-flex justify-content-center">
        <div class="col-md-2">
            @include('tweet.components.leftbar')
        </div>
        <div class="col-md-5" id="tweets">
            @yield('main')
        </div>
        <div class="col-md-2">
            @include('tweet.components.rightbar')
        </div>
    </div>

    <form id="deleteTweetForm" method="POST">
        {{ method_field("DELETE") }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
    <script src="{{ asset('js/destroy_tweet.js') }}"></script>
@endsection
