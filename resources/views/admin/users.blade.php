@extends('../layouts/app')

@section('content')
    <div class="row mt-2 container">
        <div class="col-md-8 card-columns">
            @foreach ($users as $user)
                <div class="card">
                    <img class="card-img-top" src="{{ $user -> avatarUrl }}" />
                    <div class="card-body text-center">
                        @if(!$user->hasRole('staff'))
                        <div class="dropdown float-right">
                          <a class="dropdown-toggle" href="#" role="button" id="dropdownUser{{ $user -> id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          </a>

                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUser{{ $user -> id }}">
                            @if($user->verified)
                                <a class="dropdown-item" href="{{ route('deactivateUser', ['id' => $user->id]) }}" data-user="{{ $user -> id }}">Disattiva</a>
                            @else
                                <a class="dropdown-item" href="{{ route('activateUser', ['id' => $user->id]) }}" data-user="{{ $user -> id }}">Attiva</a>
                            @endif
                          </div>
                        </div>
                        @endif
                        <h4 class="card-title">{{ $user->name }} <small>
                            @if($user->hasRole('staff'))
                                <span class="badge badge-warning">Staff</span>
                            @elseif ($user ->hasRole('scout'))
                                <span class="badge badge-danger">Scout</span>
                            @elseif (!$user -> verified)
                                <span class="badge badge-dark">VP</span>
                            @endif
                        </small></h4>
                        <div class="d-flex justify-content-around">
                            <p><i class="fa fa-twitter"></i> {{ $user -> tweets -> count() }}</p>
                            <p><i class="fa fa-comment"></i> {{ $user -> comments -> count() }}</p>
                            <p><i class="fa fa-heart"></i> {{ $user -> votes -> count() }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
